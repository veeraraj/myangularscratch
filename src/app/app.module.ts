import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import {
	HeaderComponent,
	FooterComponent,
	MainComponent,
	NavbarComponent,
	ErrorComponent
} from './layout';
import { LayoutRoutingModule } from './layout/layout-routing.module';
import {DashboardModule} from './dashboard/dashboard.module'
import { AboutModule } from './about/about.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AboutComponent } from './about/about.component';


@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		FooterComponent,
		NavbarComponent,
		ErrorComponent,
		MainComponent,
		DashboardComponent,
		AboutComponent
	],
	imports: [
		LayoutRoutingModule,
		DashboardModule,
		AboutModule,
		BrowserModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule { }
