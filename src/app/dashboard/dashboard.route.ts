import { Routes } from "@angular/router";
import { DashboardComponent } from './dashboard.component';

import { MainComponent } from '../layout';

export const HOME_ROUTE: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: 'dashboard',
				component: DashboardComponent,
			}
		]
	}
];