import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import {navbarRoute,topHeaderRoute} from '../app.route';
import {errorRoute} from './';

const LAYOUT_ROUTES=[
	navbarRoute,
	topHeaderRoute,
	...errorRoute
];

@NgModule({
	imports:[
		RouterModule.forRoot(LAYOUT_ROUTES)
	],
	exports:[
		RouterModule
	]
})

export class LayoutRoutingModule{}