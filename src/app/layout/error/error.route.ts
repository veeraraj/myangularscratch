import { ErrorComponent } from './error.component';
import { Routes } from '@angular/router';

export const errorRoute:Routes=[
	{
		path:'error',
		component:ErrorComponent,
		data:{
			authorities:[],
			pageTitle:'error.title'
		},
	},{
		path:'acccessdenied',
		component:ErrorComponent,
		data:{
			authorities:[],
			pageTitle:'error.title',
			error403:true
		}
	}
]