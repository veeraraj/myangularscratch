export * from './error/error.component';
export * from './footer/footer.component';
export * from './header/header.component';
export * from './navbar/navbar.component';
export * from './main/main.component'
export * from './error/error.route';
