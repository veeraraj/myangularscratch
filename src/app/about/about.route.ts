import { AboutComponent } from './about.component';
import { Routes } from "@angular/router";

import { MainComponent } from '../layout';

export const ABOUT_ROUTE: Routes = [
	{
		path: '',
		component: MainComponent,
		children: [
			{
				path: 'about',
				component: AboutComponent,
			}
		]
	}
];