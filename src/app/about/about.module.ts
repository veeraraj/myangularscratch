import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ABOUT_ROUTE } from './about.route';

@NgModule({
  imports: [
    RouterModule.forRoot(ABOUT_ROUTE)
  ],
  declarations: []
})

export class AboutModule { }

