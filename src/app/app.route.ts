import { Route } from '@angular/router';

import { NavbarComponent, HeaderComponent } from './layout';

export const navbarRoute: Route = {
	path: '',
	component: NavbarComponent,
	outlet: 'navbar'
};

export const topHeaderRoute: Route = {
	path: '',
	component: HeaderComponent,
	outlet: 'topheader'
};